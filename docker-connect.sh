#!/usr/bin/env bash

DOCKERID=$(docker ps | grep _jenkins_ | cut -d' ' -f1)

docker exec -it "$DOCKERID" /bin/bash
