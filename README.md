# jenkins docker container

## Table of Contents

  - [Installation](#installation)

## Installation

Run `docker-compose build` to pulling the image and building the container.

## Usage

To start the docker-container run the command `docker-compose up`. (Shows log output)
If you want to start the container in the Background use `docker-compose up -d`.
