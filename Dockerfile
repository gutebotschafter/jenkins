# jenkins
FROM php:7.3

USER root

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -qq && apt-get install -y --force-yes software-properties-common curl apt-transport-https \
    ca-certificates gnupg2 wget

RUN wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | apt-key add -
RUN sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
RUN dpkg --configure -a

RUN mkdir -p /usr/share/man/man1
RUN mkdir -p /root/.ssh
RUN mkdir -p /root/scripts

RUN apt-get update && apt-get install -y --force-yes \
            openjdk-8-jdk \
            openjdk-8-jre \
            jenkins \
            install build-essential \
            libkrb5-dev \
            git \
            vim \
            htop \
            mc \
            zlib1g-dev \
            libzip-dev \
            libicu-dev \
            g++ \
    		libfreetype6-dev \
    		libjpeg62-turbo-dev \
    		libmcrypt-dev \
    		libpng-dev \
            graphicsmagick \
            pkg-config \
            patch;

ADD https://git.archlinux.org/svntogit/packages.git/plain/trunk/freetype.patch?h=packages/php /tmp/freetype.patch
RUN docker-php-source extract; \
    cd /usr/src/php; \
    patch -p1 -i /tmp/freetype.patch; \
    rm /tmp/freetype.patch

RUN docker-php-ext-install zip \
    && docker-php-ext-enable zip \
    && docker-php-ext-install mysqli \
    && docker-php-ext-enable mysqli \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl \
    && docker-php-ext-configure gd --with-gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd

RUN curl -sL https://deb.nodesource.com/setup_11.x | bash - && apt-get install -y nodejs

COPY ./jenkins/scripts/install-composer.sh /root/install-composer.sh

RUN sh -c /root/install-composer.sh

ENTRYPOINT service jenkins start && /bin/bash
